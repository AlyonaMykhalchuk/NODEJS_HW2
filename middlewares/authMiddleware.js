const jwt = require('jsonwebtoken');
const {secret} = require('../config/auth');

module.exports = (request, response, next) => {
    const authHeader = request.headers['authorization'];
    if (!authHeader) {
        return response.status(401).json({status: 'No authorization found'});
    }
    const [, jwtToken] = authHeader.split(' ');
    try {
        request.user = jwt.verify(jwtToken, secret);
        next();
    } catch (err) {
        return response.status(401).json({status: 'Invalid JWT'});
    }
};
