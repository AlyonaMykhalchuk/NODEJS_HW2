RD LAB FE14 NODEJS HOMEWORK №2

Use <b>NodeJS</b> to implement CRUD API for notes.

<h4>Requirements:</h4>

• Use express to implement web-server;

• Use express Router for scaling your app and MVC pattern to organise project
structure;

• Use jsonwebtoken package for jwt authorization;

• Ability to run server on port which is defined as PORT env variable;

• Mandatory npm start script.


<h4> Acceptance criteria:</h4>

• Ability to register users;

• User is able to login into the system;

• User is able to view only personal notes;

• User is able to add, delete personal notes;

• User can check/uncheck any note;

• User can manage notes and personal profile only with valid JWT token in
request;

• Gitlab repo link and project id are saved in Google spreadsheets by link:
https://docs.google.com/spreadsheets/d/
1n91rWSw76qjC2KIHXd5CpdegQKaBbolVo_vbByCB5hk/edit#gid=0


<h4>Optional criteria:</h4>

• User is able to view his profile info;

• User can change his profile password;

• User can delete personal account;

• Ability to edit personal notes text;

• Simple UI for your application(would be a big plus).
