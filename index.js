const express = require('express');
const mongoose = require('mongoose');
const {PORT} = require('./config/server');
const path = require('path');
const {DB_PASSWORD,DB_USER,DB_NAME} = require('./config/database');
const expressHandlebar = require('express-handlebars');
const userRouter = require('./routers/userRouter');
const authRouter = require('./routers/authRouter');
const noteRouter = require('./routers/noteRouter');
const viewsRouter = require('./routers/viewsRouter');
const authMiddleware = require('./middlewares/authMiddleware');
const app = express();
const hbs = expressHandlebar.create({
    defaultLayout: 'main',
    extname: 'hbs'
});


app.engine('hbs', hbs.engine);
app.set('view engine', 'hbs');
app.set('views', 'views');

mongoose.connect(`mongodb+srv://${DB_USER}:${DB_PASSWORD}@cluster0.rc2v3.mongodb.net/${DB_NAME}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
}).then(res => console.log('DB connected'));
app.use(express.urlencoded({extended: true}));
app.use(express.json());
app.use(express.static(path.join(__dirname,'public')));
app.use('/api', authRouter);
app.use('/api', viewsRouter);
app.use(authMiddleware);
app.use('/api/users/me', userRouter);
app.use('/api/notes', noteRouter);



app.listen(PORT, () => {
    console.log(`Server listens on ${PORT} port`);
});

