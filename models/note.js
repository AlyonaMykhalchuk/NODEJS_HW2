const mongoose = require('mongoose');

module.exports = mongoose.model('note', {
    userId:{
        type: String
    },
    completed: {
        type: Boolean,
        default: false
    },
    text: {
        required: true,
        type: String
    },
    createdDate: {
        type: Date,
        default: Date.now()
    }
});
