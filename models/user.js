const mongoose = require('mongoose');
module.exports = mongoose.model('user', {
    username: {
        required: true,
        type: String,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    createdDate: {
        type: String,
        default: new Date()
    },
});
