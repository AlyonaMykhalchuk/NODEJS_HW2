const express = require('express');
const router = express.Router();

router.get('/mynotes', (request, response) => {
    response.render('notes', {
        title: 'Notes',
    })
});

router.get('/profile',(request, response) => {
    response.render('profile');
});
router.get('/', (request, response) => {
    response.render('index', {
        title: 'Home'
    })
});
router.get('/login', (request, response) => {
    response.render('login', {
        title: 'Login',
    })
});
router.get('/register', (request, response) => {
    response.render('register', {
        title: 'Register',
    })
});
module.exports = router;
