const express = require('express');
const router = express.Router();
const {addNote, getNotes, getNoteById, updateNote, changeStatus, deleteNote} = require('../controllers/noteController');


router.get('/', getNotes);
router.post('/', addNote);
router.get('/:id', getNoteById);
router.put('/:id', updateNote);
router.patch('/:id', changeStatus);
router.delete('/:id', deleteNote);


module.exports = router;
