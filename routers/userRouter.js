const express = require('express');
const router = express.Router();
const {getUser,deleteUser,editPassword} = require('../controllers/userController');

router.get('/', getUser);
router.delete('/', deleteUser);
router.patch('/', editPassword);

module.exports = router;



