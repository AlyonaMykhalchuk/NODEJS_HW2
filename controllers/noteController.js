const Note = require('../models/note');

module.exports.getNotes = (request, response) => {
    const userId = request.user._id;
    Note.find({userId}).exec()
        .then(data => {
            if (!data) {
                return response.status(400).json({message: 'No notes found for this user'});
            }
            response.status(200).json({notes: data});
        })
        .catch(err => {
            response.status(500).json({message: err.message})
        })
};
module.exports.addNote = (request, response) => {
    const {text} = request.body;
    const userId = request.user._id;
    if (!text) {
        return response.status(400).json({message: 'Invalid parameters'});
    }
    const note = new Note({text, userId});
    note.save()
        .then(() => {
            response.status(200).json({message: 'success'});
        })
        .catch(err => {
            response.status(500).json({message: err.message});
        });
};

module.exports.getNoteById = (request, response) => {
    const _id = request.params.id;
    const userId = request.user._id;
    Note.findOne({_id}).exec()
        .then((note) => {
            if (!note) {
                return response.status(404).json({message: 'Invalid note\'s id'});
            }
            if (note.userId !== userId){
                return response.status(400).json({message: 'You don\'t have access to this note'});
            }
                response.status(200).json({note: note})
        })
        .catch(err => {
            response.status(500).json({message: err.message})
        })
};

module.exports.updateNote = (request, response) => {
    const _id = request.params.id;
    const {text} = request.body;
    if (!text) {
        return response.status(400).json({message: 'Enter text of note'});
    }
    Note.findByIdAndUpdate(_id, {text: text}).exec()
        .then(() => {
            response.status(200).json({message: 'success'})
        })
        .catch(err => {
            response.status(500).json({message: err.message})
        })
};

module.exports.changeStatus = (request, response) => {
    Note.findById(request.params.id).exec()
        .then(note => {
            if (note.userId !== request.user._id) {
                return response.status(400).json({message: 'You don\'t have access to this note'});
            }
            if (!note) {
                return response.status(400).json({message: 'Note not found'});
            }
            note.completed = !note.completed;
            note.save()
                .then(() => {
                    response.json({message: 'Success'});
                })
                .catch(err => {
                    response.status(500).json({message: err.message});
                });
        })
        .catch(err => {
            response.status(500).json({message: err.message});
        });


};
module.exports.deleteNote = (request, response) => {
    const _id = request.params.id;
    Note.findByIdAndDelete({_id}).exec()
        .then((note) => {
            if (!note) {
                return response.status(404).json({message: 'Invalid note\'s id'});
            }
            response.status(200).json({message: 'success'})
        })
        .catch((err) => {
            response.status(500).json({message: err.message})
        })
};

