const User = require('../models/user');
const jwt = require('jsonwebtoken');
const {secret, SALT} = require('../config/auth');

module.exports.registerUser = (request, response) => {
    const {username, password} = request.body;
    const user = new User({username, password});
    user.save()
        .then(() => {
            response.status(200).json({message: 'success'});
        })
        .catch(err => {
            if (!username || !password) {
                response.status(400).json({message: err.message});
            }
            response.status(500).json({message: err.message});
        });
};
module.exports.loginUser = (request, response) => {
    const {username, password} = request.body;
    if (!username || !password) {
        return response.status(400).json({message: err.message});
    }
    User.findOne({username, password}).exec()
        .then(user => {
            if (!user) {
                return response.status(400).json({message: 'No user with such username and password'});
            }
            return response.status(200).json({message: 'success', token: jwt.sign(JSON.stringify(user), secret)});

        })
        .catch(err => {
            response.status(500).json({message: err.message});
        });
};

