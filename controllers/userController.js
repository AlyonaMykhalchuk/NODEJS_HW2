const User = require('../models/user');
const Note = require('../models/note');

module.exports.getUser = (request, response) => {
    const {_id, username, createdDate} = request.user;
    User.findById({_id}).exec()
        .then(user => {
            if (!user) {
                return response.status(400).json({message: 'User not found'});
            }
            response.status(200).json({
                user: {_id, username, createdDate}
            });
        })
        .catch(err => {
            response.status(500).json({message: err.message});
        });

};

module.exports.deleteUser = (request, response) => {
    const {_id} = request.user;

    User.findByIdAndDelete({_id}).exec()
        .then((user) => {
            if (!user) {
                return response.status(400).json({message: 'User not found'});
            }
            response.status(200).json({message: 'success'})
        })
        .catch((err) => {
            response.status(500).json({message: err.message})
        });
    Note.remove({userId: _id}).exec()
        .then((notes) => {
            if (!notes) {
                return response.status(400).json({message: 'There are no notes for current user'});
            }
            response.status(200).json({message: 'success'})
        })
        .catch((err) => {
            response.status(500).json({message: err.message})
        })
};
module.exports.editPassword = (request, response) => {
    const {oldPassword, newPassword} = request.body;
    const {_id} = request.user;
    if (!oldPassword) {
        return response.status(400).json({message: 'No old password found'});
    }
    if (!newPassword) {
        return response.status(400).json({
            message: 'No new password  found'
        });
    }

    User.findByIdAndUpdate({_id}, {password: newPassword}).exec()
        .then(user => {
            if(!user){
                return response.status(400).json({message: 'No such register user'});
            }
            if (user.password !== oldPassword) {
                return response.status(400).json({message: 'Invalid Password'});
            }
            return response.json({message: 'Success'})
        })
        .catch((err) => {
            response.status(500).json({message: err.message});
        });
};

